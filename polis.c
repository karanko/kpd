#include "m_pd.h"

#define MAXSTEPS 8
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

static t_class *polis_class;

typedef struct _polis
{
    t_object x_obj;
    int i_step;
    t_float pitches[MAXSTEPS];
    t_float pulses[MAXSTEPS];
    t_float gatemodes[MAXSTEPS];
    t_float current_pitch_value;
    int current_step;
    t_outlet *f_out, *a_out, *b_out, *d_out;
} t_polis;

//TODO: make these generic
void polis_setpitch(t_polis *x, t_floatarg index, t_floatarg value)
{
    // error("polis_setpitch %f %f", index, value);
    if (index > MAXSTEPS || index < 0)
        return;
    x->pitches[(int)index] = value;
}
void polis_setpulse(t_polis *x, t_floatarg index, t_floatarg value)
{
    // error("polis_setpulse %f %f", index, value);
    if (index > MAXSTEPS || index < 0)
        return;
    x->pulses[(int)index] = (int)value;
}
void polis_setgatemode(t_polis *x, t_floatarg index, t_floatarg value)
{
    // error("polis_setgatemode %f %f", index, value);
    if (index > MAXSTEPS || index < 0)
        return;
    x->gatemodes[(int)index] = (int)value;
}

void polis_bang(t_polis *x)
{
    polis_float(x, x->i_step);
    x->i_step++;
}
void polis_float(t_polis *x, t_floatarg step)
{
    x->i_step = (int)step;
    //TODO: is the step getting updated?? HOW???
    int length = 0;
    for (int i = 0; i < MAXSTEPS; i++)
        length += x->pulses[i];

    int pos = x->i_step % length;

    int pulse_sub_step = 0;
    int index = 0;
    for (int step = 0; step < pos && index < MAXSTEPS; step++)
    {
        pulse_sub_step++;
        if (x->pulses[index] < pulse_sub_step)
        {
            pulse_sub_step = 0;
            index++;
        }
    }

    int should_bang = 0;
    int gatemode = (int)x->gatemodes[index];

    switch (gatemode)
    {
    case 2:
        should_bang = pulse_sub_step == 1;
        break;
    case 3:
        should_bang = (pulse_sub_step + 1) % 2;
        break;
    case 4:
        should_bang = 1;
        break;
    default:
        should_bang = 0;
        break;
    }

    if (should_bang == 1)
    {
        x->current_pitch_value = x->pitches[index];
        outlet_float(x->f_out, x->current_pitch_value);
        outlet_bang(x->a_out);
    }

    if (pos == length - 1)
        outlet_bang(x->b_out);
}
void polis_dump(t_polis *x)
{
}
void polis_reset(t_polis *x)
{
    x->i_step = 0;
}
void polis_clear(t_polis *x)
{
    for (int i = 0; i < MAXSTEPS; i++)
        x->pitches[i] = x->current_pitch_value;
}

void *polis_new(t_symbol *s, int argc, t_atom *argv)
{
    t_polis *x = (t_polis *)pd_new(polis_class);

    x->current_pitch_value = 0;
    x->current_step = 0;

    polis_reset(x);
    polis_clear(x);

    x->f_out = outlet_new(&x->x_obj, &s_float);
    x->a_out = outlet_new(&x->x_obj, &s_bang);
    x->b_out = outlet_new(&x->x_obj, &s_bang);

    return (void *)x;
}
void polis_free(t_polis *x)
{
    outlet_free(x->f_out);
    outlet_free(x->a_out);
    outlet_free(x->b_out);
}

void polis_setup(void)
{
    polis_class = class_new(gensym("polis"),
                            (t_newmethod)polis_new,
                            (t_newmethod)polis_free,
                            sizeof(t_polis),
                            CLASS_DEFAULT,
                            A_GIMME,
                            0);

    class_addbang(polis_class, (t_method)polis_bang);
    class_addfloat(polis_class, (t_method)polis_float);

    class_addmethod(polis_class, (t_method)polis_reset, gensym("reset"), 0);
    class_addmethod(polis_class, (t_method)polis_clear, gensym("clear"), 0);
    class_addmethod(polis_class, (t_method)polis_dump, gensym("dump"), 0);

    class_addmethod(polis_class,
                    (t_method)polis_setpitch, gensym("setpitch"),
                    A_FLOAT, A_FLOAT,
                    0);
    class_addmethod(polis_class,
                    (t_method)polis_setpulse, gensym("setpulse"),
                    A_FLOAT, A_FLOAT,
                    0);
    class_addmethod(polis_class,
                    (t_method)polis_setgatemode, gensym("setgatemode"),
                    A_FLOAT, A_FLOAT,
                    0);
}
