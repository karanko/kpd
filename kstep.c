#include "m_pd.h"

#define MAXSTEPS 64

static t_class *kstep_class;

typedef struct _kstep
{
    t_object x_obj;
    int i_length;
    int i_step;
    t_float steps[MAXSTEPS];
    t_outlet *f_out, *a_out, *b_out;
} t_kstep;

t_float kstep_getstep(t_kstep *x, int index)
{
    int i = (x->i_step % x->i_length);
    return x->steps[i];
}
void kstep_setstep(t_kstep *x, t_floatarg index, t_floatarg value)
{
    x->steps[((int)index % MAXSTEPS)] = value;
}

void kstep_bang(t_kstep *x)
{
    kstep_float(x);
    x->i_step++;
}
void kstep_float(t_kstep *x)
{
    outlet_float(x->f_out, kstep_getstep(x, (x->i_step % x->i_length)));

    if (kstep_getstep(x, (x->i_step % x->i_length)) != 0)
        outlet_bang(x->b_out);

    if ((x->i_step % x->i_length) == x->i_length - 1)
        outlet_bang(x->b_out);
}
void kstep_reset(t_kstep *x)
{
    x->i_step = 0;
}
void kstep_clear(t_kstep *x)
{
    for (int i = 0; i < MAXSTEPS; i++)
        x->steps[i] = 0;
}

void *kstep_new(t_symbol *s, int argc, t_atom *argv)
{
    t_kstep *x = (t_kstep *)pd_new(kstep_class);

    if (argc == 1)
        x->i_length = atom_getfloat(argv);
    else
        x->i_length = MAXSTEPS;

    if (x->i_length > MAXSTEPS)
        x->i_length = MAXSTEPS;

    kstep_reset(x);
    kstep_clear(x);

    x->f_out = outlet_new(&x->x_obj, &s_float);
    x->a_out = outlet_new(&x->x_obj, &s_bang);
    x->b_out = outlet_new(&x->x_obj, &s_bang);

    return (void *)x;
}
void kstep_free(t_kstep *x)
{
    outlet_free(x->f_out);
    outlet_free(x->a_out);
    outlet_free(x->b_out);
}

void kstep_setup(void)
{
    kstep_class = class_new(gensym("kstep"),
                            (t_newmethod)kstep_new,
                            (t_newmethod)kstep_free,
                            sizeof(t_kstep),
                            CLASS_DEFAULT,
                            A_GIMME, 0);

    class_addbang(kstep_class, kstep_bang);
    class_addfloat(kstep_class, (t_method)kstep_float);

    class_addmethod(kstep_class, (t_method)kstep_reset, gensym("reset"), 0);
    class_addmethod(kstep_class, (t_method)kstep_clear, gensym("clear"), 0);

    class_addmethod(kstep_class,
                    (t_method)kstep_setstep, gensym("setstep"),
                    A_DEFFLOAT, A_DEFFLOAT, 0);

    class_sethelpsymbol(kstep_class, gensym("kstep"));
}
